//Initialisierung der Sensoren und Motoren mit Standardbelegung
#pragma config(Sensor, S1, touchSensor, sensorEV3_Touch)
#pragma config(Sensor, S2, gyroSensor, sensorEV3_Gyro)
#pragma config(Sensor, S3, colorSensor, sensorEV3_Color)
#pragma config(Sensor, S4, sonarSensor, sensorEV3_Ultrasonic)
#pragma config(Motor, motorA, armMotor, tmotorEV3_Medium, PIDControl, encoder)
#pragma config(Motor, motorB, leftMotor, tmotorEV3_Large, PIDControl, driveLeft, encoder)
#pragma config(Motor, motorC, rightMotor, tmotorEV3_Large, PIDControl, driveRight, encoder)
//*!!Code automatically generated by 'ROBOTC' configuration wizard               !!*//
// Kann auch über "Motor and Sensor Setup"-Button oder Codeanpassung verändert werden

/*
Autor:               Bock/Feike/Henning
Pfad/Datei:          start.c
Zugehörig zu:        Einfuehrung in LEGO Mindstorms EV3
Beschreibung:        Erstes Programm in ROBOTC
Erstelldatum:        2018-02-12
*/

// Nummer 9

int stuhlfarbe = -1;
bool stuhlAngehoben = false;

void EckenfahrerLinienende();

void AusgabeD(string TextZeile);

void Stuhlhaben();

void Stuhlnicht();

void Kreuzung(int Stuhlfarbe, bool buhlStuhl);

void Drehen(int Grad);

void GeradeausFahren(int geschwindigkeit);

void Stoppen();

task main() {
    bool warten = true;
    while (true) {
        while (warten) {
            warten = !getTouchValue(S1);
        }
        resetGyro(S2);
        getColorName(S3);
        sleep(100);
        stuhlfarbe = getColorName(S3);
        Stuhlhaben();
        stuhlAngehoben = true;
        Drehen(70);
        EckenfahrerLinienende();
        Kreuzung(stuhlfarbe, stuhlAngehoben);
        EckenfahrerLinienende();
        Stuhlnicht();
        Drehen(150);
        EckenfahrerLinienende();
        Kreuzung(stuhlfarbe, stuhlAngehoben);
        EckenfahrerLinienende();
        Drehen(90);
        warten = true;
    }
}

void GeradeausFahren(int geschwindigkeit) {
    setMotorSpeed(motorB, geschwindigkeit);
    setMotorSpeed(motorC, geschwindigkeit);
}

void Stoppen() {
    setMotorSpeed(motorB, 0);
    setMotorSpeed(motorC, 0);
}

void Drehen(int Grad) {
    string text = "Drehen";
    AusgabeD(text);
    long Drehung = 0;
    resetGyro(S2);
    while (Grad > Drehung) {
        setMotorSpeed(motorB, 20);
        setMotorSpeed(motorC, -20);
        sleep(100);
        Drehung = getGyroDegrees(S2);
    }
    Stoppen();
    resetGyro(S2);
}

void EckenfahrerLinienende() {
    string text = "Ecke";
    AusgabeD(text);
    int iGrad = 0;
    int iHelligkeit = 0;
    bool bLinienEnde = false;
    getColorReflected(S3);
    sleep(100);
    while (!bLinienEnde) {
        iHelligkeit = getColorReflected(S3);
        if (iHelligkeit < 45) {
            GeradeausFahren(20);
        } else {
            resetGyro(S2);
            iGrad = getGyroDegrees(S2);
            while (iGrad < 55 && iHelligkeit > 45) {
                setMotorSpeed(motorB, 10);
                setMotorSpeed(motorC, -10);
                iHelligkeit = getColorReflected(S3);
                iGrad = getGyroDegrees(S2);
            }
            while (iGrad > -55 && iHelligkeit > 45) {
                setMotorSpeed(motorB, -10);
                setMotorSpeed(motorC, 10);
                iHelligkeit = getColorReflected(S3);
                iGrad = getGyroDegrees(S2);
            }
            while (iGrad < 0 && iHelligkeit > 45) {
                setMotorSpeed(motorB, 10);
                setMotorSpeed(motorC, -10);
                iGrad = getGyroDegrees(S2);
                bLinienEnde = true;
            }
        }
    }
    text = "Ecke fertig";
    AusgabeD(text);
    Stoppen();
}

void AusgabeD(string TextZeile) {
    eraseDisplay();
    //displayBigTextLine(1,"Distanz: %d cm",getUSDistance(S4));
    //displayBigTextLine(3,"Taster: %d ",getTouchValue(S1));
    displayBigTextLine(1, "Gyro: %d Grad", getGyroDegrees(S2));
    //getColorName(S3);
    //sleep(100);
    //displayBigTextLine(7,"Farbnr: %d ",getColorName(S3));
    //getColorHue(S3);
    //sleep(100);
    //displayBigTextLine(9,"Farbwert: %d ",getColorHue(S3));
    // getColorReflected(S3);
    // sleep(100);
    displayBigTextLine(5, "Helligkeit: %d ", getColorReflected(S3));
    string text = "S: ";
    text += TextZeile;
    displayBigTextLine(9, text);
}

void Stuhlhaben() {
    string text = "Stuhlhaben";
    AusgabeD(text);
    setMotorSpeed(motorA, -25);
    sleep(200);
    setMotorSpeed(motorA, 0);
    GeradeausFahren(30);
    sleep(1000);
    Stoppen();
    setMotorSpeed(motorA, 25);
    sleep(300);
    setMotorSpeed(motorA, 0);
    stuhlAngehoben = true;
    GeradeausFahren(-30);
    sleep(500);
    Stoppen();
}

void Stuhlnicht() {
    string text = "Stuhlnicht";
    AusgabeD(text);
    GeradeausFahren(30);
    sleep(400);
    setMotorSpeed(motorA, -15);
    sleep(350);
    setMotorSpeed(motorA, 0);
    GeradeausFahren(-30);
    sleep(500);
    Stoppen();
    setMotorSpeed(motorA, 15);
    sleep(300);
    setMotorSpeed(motorA, 0);
    stuhlAngehoben = false;
}

void Kreuzung(long Stuhlfarbe, bool buhlStuhl) {
    string text = "Kreuzung";
    AusgabeD(text);
    GeradeausFahren(30);
    sleep(800);
    Stoppen();
    switch (Stuhlfarbe) {
        case 5:
            // Rot
            if (buhlStuhl) {
                Drehen(255);
            } else {
                Drehen(75);
            }
            break;
        case 4:
            // Gelb
            break;
        case 1:
            // Schwarz
            if (buhlStuhl) {
                GeradeausFahren(30);
                sleep(300);
                Stoppen();
                Drehen(75);
            } else {
                Drehen(255);
            }
            break;
    }
    //Geradeausfahren
    GeradeausFahren(30);
    sleep(800);
}
